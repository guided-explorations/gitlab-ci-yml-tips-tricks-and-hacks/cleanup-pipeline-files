# Cleanup Pipeline Files

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/-/ide/project/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/cleanup-pipeline-files)

## IMPORTANT LIMITATIONS

In a cluster of runners there is no guarantee that the cleanup job will run on the same runner host machine as the previous build stages. You would have to peg all jobs in the pipeline to a single runner - which negates many of the benefits of running scaled clusters.

Additionally, indescriminate deletes can remove files being used by a parallel pipeline for the same project being processed on the same runner - while this is more of a problem for busy repositories there are simply no guarantees that this is not occuring.

To accomodate these challenges, this example supports two modes:

### CLEANUPMETHOD = 'all_older_files'
- Default and is what should be used if you do not have needs outlined in the next method.
- Everytime this runs it will prune files older than 30 days from the entire directory tree under $CI_BUILDS_DIR - not just the current job.
- Positive - should help maintain the entire cluster's build directories on instances and do so with only incremental impact on each job that runs cleanup.
- Negative - can run long.
- Can be set to run on a schedule in a dedicated project, **however** it must be scheduled very often to hit all instances in a cluster due to randomized job scheduling (based on runner job polling) used by GitLab. Reliable cluster coverage is more likely when running it after every pipeline.

### CLEANUPMETHOD = 'just_this_job'
- most efficient in situations where it is well known that the same runner instance will run an entire pipeline - e.g. there is only one shell runner or a pipeline that is completely pegged to a known runner instance (however, you should not do such pegging just to gain this cleanup capability - use 'all_older_files' instead).

### Uses and features for this example:

GitLab jobs and pipelines do not clean up the temporary build folders that are created. On Shell Executors this eventually ends up causing the disk to fill up.

Supports:
- bash and powershell
- uses special stage CleanupFileVariables
- must be enabled using CLEANUPPIPELINEFILES=true
- use VERBOSESWITCH=true to see output of what is deleted (except .git folder which could fill a lot of log space and take too much time) - special work around for powershell verbose support as the runner logs do not surface all console io streams
- use FORCE=true to delete even when the entire environment will be discarded by the runner. Under normal circumstances, this code will not delete when $CI_DISPOSABLE_ENVIRONMENT is 'true' as it wastes compute because the entire environment is disposed of (e.g. when building using containers.)  This parameter always forces a delete - can be used for testing.
- Docker system prune for shell runners with docker or if you configure a shell runner on docker executor machines to cleanup when using a GitLab Docker executor pull_policy other than 'always'

## Demonstrates These Design Requirements, Desirements and AntiPatterns

* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Using GitLab CI Predefined Variables to understand the environment (using CI_DISPOSABLE_ENVIRONMENT to know if the entire runner is thrown away after use - negating the need to delete files).
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Using `rules:if:` to prevent a job from being scheduled at all if it's switch is not enabled (CLEANUPPIPELINEFILES is the switch in this case).
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Using `variables:name:value` and `variables:name:description` to create a "Run Pipeline" form.
* **Development Pattern:** Providing a verbose output capability for debugging.
* **Development Pattern:** Providing code for both shell languages directly support by GitLab runner.
* **Development Pattern:** Providing a force switch to see the code in action when testing on GitLab.com and for other situations where it might be needed.

### Cross References and Documentation

- Formal product functionality to replace this Guided Exploration is underway and can be tracked on this issue: [Shell executor doesn't clean up build directories](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3856)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2021-05-17

* **GitLab Version Released On**: 13.8

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **Tested On**: 
  * GitLab Docker-Executor Runner using bash:latest container

* **References and Featured In**:
  * [Scheduled and On Demand Tasks Video Walkthrough Demo](https://youtu.be/iGZ-xY1f-B0)
