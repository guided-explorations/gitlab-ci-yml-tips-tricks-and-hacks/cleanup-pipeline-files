
# How I Found This

This file allows those who find this solution to give a brief phrase of what led them to using some or all of the code in the example.  This increases search engine optimization (SEO) by enabling community curation of search terms or summaries of the "sample code quest" that was satisfied by this code - whether the code was directly intended to help in that way or not.

If you found this example helpful in ways that are not already described in the README.md, the code comments or in the below, please feel free to fork the project and do an MR with your addition to the below so that others searching for your use case can also find this example!

**2021-05-24**: @DarwinJS: I didn't realize GitLab could do basic forms when running pipelines until I found this code: https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/cleanup-pipeline-files/-/blob/main/.gitlab-ci.yml#L9-26

**2021-04-21**: @GitLabHandle: Was looking for a way to reliably cleanup files for a cluster of shell runners without deleting files that were currently being used. Deleting only files that are older seems to be right for me as shown here: https://gitlab.com/-/ide/project/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/cleanup-pipeline-files/tree/main/-/.gitlab-ci.yml/#L8
